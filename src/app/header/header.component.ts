import { Component } from '@angular/core';
import { ParticipantService } from '../participant-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
}
